## Basic cmakelist.txt for your PGR project (Mac and Linux)

All you need is to change this line (21): 

```cmake


# HERE edit your path to pgr framework folder
set(PATH_TO_PRG_FRAMEWORK "/pgr-framework")


```

Change the string to the path to root of the pgr-framework.

Put this cmakelist.txt into the folder with your C++ project and your project is ready to compile.
